import { Component, Input, AfterViewInit, ElementRef } from '@angular/core';
//import { DebuggerData } from '../puzzle-games/models/debugger-data';
import { Square } from '../puzzle-games/models/square';
import { DebuggerData } from '../puzzle-games/models/debugger-data';

@Component({
  selector: 'ngx-debugger',
  templateUrl: './debugger.component.html',
  styleUrls: ['./debugger.component.scss']
})
export class DebuggerComponent {
  @Input() public debugData: DebuggerData[] = [];
  public currentValue: DebuggerData = null;

  constructor(private el: ElementRef) { }

  pushToDebugData(value: DebuggerData) {
    this.debugData.push(value);
    this.currentValue = value;
  }

  downScrollControl() {
    let list = this.el.nativeElement.querySelector('nb-card-body');
    list.scrollTop = list.scrollHeight;
  }
  
}
