import { Component, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { GameboardDrawService } from '../services/gameboard-draw.service';
import { GameboardConfig } from '../puzzle-games/interfaces/gameboard-config';
import { Square } from '../puzzle-games/models/square';
import { DebuggerComponent } from '../debugger/debugger.component';
import { AnimationVelocityComponent } from '../animation-velocity/animation-velocity.component';

@Component({
    selector: 'ngx-gameboard',
    templateUrl: './gameboard.component.html'
})
export class GameboardComponent {
   // @ViewChild(DebuggerComponent) protected dbcComponent: DebuggerComponent;
   // @ViewChild(AnimationVelocityComponent) protected avComponent: AnimationVelocityComponent;

    protected drawServiceMediator: GameboardDrawService;
    
    protected letters;
    protected size: number;
    protected rows: number;
    protected cols: number;
    protected lettersByPosition: string[];
    protected width: number = 600;
    protected x: number = 10;
    protected y: number = 10;
    protected rectangleWidth: number;
    protected used: boolean[];
    
    constructor(
        protected renderer2: Renderer2,
        protected el: ElementRef
    ) {
        this.drawServiceMediator = new GameboardDrawService(this.renderer2, this.el);
        this.lettersByPosition = [];
        this.init();
    }

    private init(): void {
        this.setUsedArray();
        console.log(this)
    }

    protected setLettersData(letters: any) {
        this.letters = letters;
        this.rows = letters.length;
        this.cols = this.size = letters[0].length;
    }

    protected setUsedArray(): void {
        this.used = Array.from(new Array(this.rows)).reduce(accumulator => {
            accumulator.push(new Array(this.cols));
            return accumulator;
        }, []);
    }

    protected draw(height: string = '500px', width: string = '500px') {
        const gameboardConfig: GameboardConfig = {
            letters: this.letters,
            lettersByPosition: this.lettersByPosition,
            rows: this.rows,
            cols: this.cols,
            size: this.size,
            width: this.width,
            x: this.x,
            y: this.y,
            rectangleWidth: this.width / this.size
        };
        
        this.drawServiceMediator.gameboardConfig = gameboardConfig;
        this.drawServiceMediator.drawBoard(height, width);
        this.drawServiceMediator.drawsService = 'topCanvas';
        this.drawServiceMediator.setBoard(); 
    }

    colour(positions: string[], colour: string): Square[] {
        return positions.map(position => new Square(position, colour))
    }

}

