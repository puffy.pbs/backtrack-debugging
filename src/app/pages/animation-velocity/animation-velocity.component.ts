import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-animation-velocity',
  templateUrl: './animation-velocity.component.html',
  styleUrls: ['./animation-velocity.component.scss']
})
export class AnimationVelocityComponent {
 @Input() speed: string = "1";
}
