export class DrawPost {
  id: string;
  game: string;
  position: number;
  date: string;
  balls: number[];
}