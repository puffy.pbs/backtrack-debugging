import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastDrawnComponent } from './last-drawn.component';

describe('LastDrawnComponent', () => {
  let component: LastDrawnComponent;
  let fixture: ComponentFixture<LastDrawnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastDrawnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastDrawnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
