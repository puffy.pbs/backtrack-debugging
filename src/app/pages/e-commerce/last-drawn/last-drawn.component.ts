import { Component } from '@angular/core';
import { DrawsService } from '../services/draws.service';
import { DrawPost } from '../models/draw-post';

@Component({
  selector: 'ngx-last-drawn',
  templateUrl: './last-drawn.component.html',
  styleUrls: ['./last-drawn.component.scss']
})
export class LastDrawnComponent {
  draws: DrawPost[] = [];
  placeholders = [];
  loading = false;
  page = 1;
  pageSize = 10;

  constructor(private drawsService: DrawsService) { }

  loadDraws() {
    if (!this.loading) {
      this.loading = true;
      this.placeholders = new Array(this.pageSize);
      this.drawsService.loadDraws(this.page, this.pageSize)
        .subscribe(lastDraws => {
          this.draws.push(...lastDraws);
          this.placeholders = [];
          this.loading = false;
          this.page++;
        });
    }
  }
}
