import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ngx-draws-post-placeholder',
  templateUrl: './draws-post-placeholder.component.html',
  styleUrls: ['./draws-post-placeholder.component.scss']
})
export class DrawsPostPlaceholderComponent {

  @HostBinding('attr.aria-label')
  label = 'Loading';
}
