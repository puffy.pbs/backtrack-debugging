import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawsPostPlaceholderComponent } from './draws-post-placeholder.component';

describe('DrawsPostPlaceholderComponent', () => {
  let component: DrawsPostPlaceholderComponent;
  let fixture: ComponentFixture<DrawsPostPlaceholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawsPostPlaceholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawsPostPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
