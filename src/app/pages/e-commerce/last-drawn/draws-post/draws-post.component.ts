import { Component, Input } from '@angular/core';
import { DrawPost } from '../../models/draw-post';

@Component({
  selector: 'ngx-draws-post',
  templateUrl: './draws-post.component.html',
  styleUrls: ['./draws-post.component.scss']
})
export class DrawsPostComponent {

  @Input() draw: DrawPost;

}
