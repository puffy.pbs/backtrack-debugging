import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawsPostComponent } from './draws-post.component';

describe('DrawsPostComponent', () => {
  let component: DrawsPostComponent;
  let fixture: ComponentFixture<DrawsPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawsPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawsPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
