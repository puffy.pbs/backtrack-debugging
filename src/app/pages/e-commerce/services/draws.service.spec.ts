import { TestBed } from '@angular/core/testing';

import { DrawsService } from './draws.service';

describe('DrawsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DrawsService = TestBed.get(DrawsService);
    expect(service).toBeTruthy();
  });
});
