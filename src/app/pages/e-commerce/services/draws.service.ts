import { Injectable } from '@angular/core';
import { DrawPost } from '../models/draw-post';
import { Observable, of } from 'rxjs';
import { delay, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  'providedIn': 'root'
})
export class DrawsService {

  constructor(private http: HttpClient) { }

  loadDraws(page: number, pageSize: number): Observable<DrawPost[]> {
    const startIndex = 1;

    this.http
      .get('/api')
      .pipe(
        tap(_ => console.log)
      );

    return this.http
      .get<DrawPost[]>('assets/data/draws.json')
      .pipe(
        map(draws => draws.splice(startIndex, pageSize)),
        delay(1500)
      )
  }
}
