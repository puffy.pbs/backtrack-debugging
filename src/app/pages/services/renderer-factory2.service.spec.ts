import { TestBed } from '@angular/core/testing';

import { RendererFactory2Service } from './renderer-factory2.service';

describe('RendererFactory2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RendererFactory2Service = TestBed.get(RendererFactory2Service);
    expect(service).toBeTruthy();
  });
});
