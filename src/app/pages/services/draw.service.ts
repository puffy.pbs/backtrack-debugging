import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DrawService {

  constructor(
    protected context: CanvasRenderingContext2D) { 
  }

  setFontStyle(fontStyle: string): void {
    this.context.font = fontStyle;
  }

  setFillStyle(fillStyle: string): void {
    this.context.fillStyle = fillStyle;
  }

  fillText(text, x, y): void {
    this.context.fillText(text, x, y);
  }

  strokeRect(x, y, width, height): void {
    this.context.strokeRect(x, y, width, height);
  }
}
