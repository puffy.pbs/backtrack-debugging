import { Injectable, RendererFactory2 } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RendererFactory2Service {
  constructor(private renderedFactory2: RendererFactory2) {

  }

   createRenderer() {
    return this.renderedFactory2.createRenderer(null, null);
  }
}
