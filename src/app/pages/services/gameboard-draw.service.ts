import { Injectable, Renderer2, ElementRef } from '@angular/core';
import { Observable, range, from } from 'rxjs';
import { map, tap, mergeMap } from 'rxjs/operators';
import { DrawSquareService } from './draw-square.service';
import { GameboardConfig } from '../puzzle-games/interfaces/gameboard-config';
import { Square } from '../puzzle-games/models/square';
import { Board } from '../puzzle-games/models/board';
import { DrawService } from './draw.service';

@Injectable({
  providedIn: 'root'
})
export class GameboardDrawService {
  private _gameboardConfig: GameboardConfig;
  private _drawsService: DrawSquareService;
  private rowsObservable: Observable<number>;
  private columnsObservable: Observable<number>;

  constructor(
    private renderer2: Renderer2,
    private el: ElementRef
  ) { }

  set drawsService(value) {
    this._drawsService = new DrawSquareService(
      this.el.nativeElement.querySelector('#' + value).getContext('2d')
    );
  }

  set gameboardConfig(value) {
    this._gameboardConfig = value;
    this.init();
  }

  private init() {
    this.setRowsObservable();
    this.setColumnsObservable();
  }

  private setRowsObservable() {
    this.rowsObservable = range(0, this._gameboardConfig.rows);
  }

  private setColumnsObservable() {
    this.columnsObservable = range(0, this._gameboardConfig.cols);
  }

  public drawBoard(height: string = '500px', width: string = '500px') {
    const boards = [new Board('bgCanvas', '0px', '0px'), new Board('topCanvas', height, width)];
        from(boards)
          .subscribe((board: Board) => {
              const parentDiv = this.el.nativeElement.querySelector('.game');
              const ctx = this.renderer2.createElement('canvas');
              this.renderer2.setAttribute(ctx, 'id', board.id);
              this.renderer2.setAttribute(ctx, 'height', board.height);
              this.renderer2.setAttribute(ctx, 'width', board.width);
              this.renderer2.appendChild(parentDiv, ctx);
          });
  }

  public setBoard(): void {
    this.setPlayField();
    this.drawLettersPlaceholder();
  }

  public highlightWords(position: string, fillStyle: string) {
    const letter = this.translate([position]);
    let positions = this._gameboardConfig.lettersByPosition[position];
    const fontStyle = '30px Arial';
    this._drawsService.highlightSquare(fontStyle, fillStyle, letter,
      positions.x, positions.y);
    /*const wordsObservable = from(words);
    wordsObservable.pipe(
      map(async (position) => {
        const letter = this.translate([position]);
        const positions = this.gameboardConfig.lettersByPosition[position];
        await this.waitFor(1000);
        this.drawService.highlightSquare('30px Arial', fillStyle, letter, 
          positions.x, positions.y);
      })
    ).subscribe();*/
  }

  waitFor(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  private setPlayField(): void {
      let startingColumn: number = this._gameboardConfig.width / this._gameboardConfig.cols;
      let z = (this._gameboardConfig.rows !== this._gameboardConfig.cols) ? 
        this._gameboardConfig.width * this._gameboardConfig.cols : 
        this._gameboardConfig.width;
      /*this._drawsService.strokeRect(this._gameboardConfig.x, this._gameboardConfig.y,
        startingColumn * this._gameboardConfig.rectangleWidth, this._gameboardConfig.width);*/
        this._drawsService.strokeRect(this._gameboardConfig.x, this._gameboardConfig.y,
          z, this._gameboardConfig.width);
  }

  private drawCols3(currentColumn: number, currentRow: number,
    startingColumn: number): void {
    this.columnsObservable.subscribe(x => {
      console.log(currentColumn, currentColumn, this._gameboardConfig.x, currentRow)
      let z = (this._gameboardConfig.rows !== this._gameboardConfig.cols) ? 
        this._gameboardConfig.width * this._gameboardConfig.cols : 
        this._gameboardConfig.width;
      //this._drawsService.drawSquare(currentColumn, currentColumn, this._gameboardConfig.x, currentRow);
      this._drawsService.drawSquare(currentColumn, currentColumn, this._gameboardConfig.x, currentRow);
      currentColumn += startingColumn;
    });
  }

  private drawCols(currentColumn: number, startingColumn: number) {
    this.rowsObservable.subscribe(() => {
      let y: number = currentColumn + (this._gameboardConfig.width - currentColumn) + this._gameboardConfig.x;
      console.log(this._gameboardConfig.x, y, currentColumn, currentColumn)
      this._drawsService.drawSquare(this._gameboardConfig.x, y, currentColumn, currentColumn);
      currentColumn += startingColumn;
    });
  }

  private drawCols2(currentColumn: number, startingColumn: number) {
    this.rowsObservable.subscribe(x => {
      console.log(x)
    //  startingColumn * this.gameboardConfig.rectangleWidth
    // pri lexica this._gameboardConfig.width + this._gameboardConfig.x
      let y: number = (this._gameboardConfig.rows !== this._gameboardConfig.cols) ? 
        (this._gameboardConfig.width + this._gameboardConfig.x) * this._gameboardConfig.cols : 
        this._gameboardConfig.width + this._gameboardConfig.x;
     // this._drawsService.drawSquare(this._gameboardConfig.x, y, currentColumn, currentColumn);
     console.log(this._gameboardConfig.x, y, currentColumn, currentColumn)
     this._drawsService.drawSquare(this._gameboardConfig.x, y, currentColumn, currentColumn);
      currentColumn += startingColumn;
    });
  }

  private addLetterByPosition(index: string, column: number, row: number) {
    this._gameboardConfig.lettersByPosition[index] = {
      x: column,
      y: row
    };
  }

  private drawLettersPlaceholder(): void {
    console.log(this._gameboardConfig)
    let startingColumn: number = this._gameboardConfig.width / this._gameboardConfig.rows;
    let currentColumn: number = startingColumn + this._gameboardConfig.x;
    let currentRow: number = this._gameboardConfig.width + this._gameboardConfig.y;
    console.log(currentColumn, currentRow, startingColumn)
    this.drawCols3(currentColumn, currentRow, startingColumn);
    currentColumn = startingColumn + this._gameboardConfig.x;
    this.drawCols2(currentColumn, startingColumn);
    this.setLettersPlaceholder(startingColumn);
  }

  private setLettersPlaceholder(startingColumn: number): void {
    this._drawsService.setFontStyle('30px Arial');
   // let rowWidth: number = this.gameboardConfig.width / (this.gameboardConfig.rows + 1);
    let incrementRowWith: number = this._gameboardConfig.width / this._gameboardConfig.rows;
    let currentColumn: number = startingColumn / 2;
    let rowWidth: number = (this._gameboardConfig.width * 0.75) / this._gameboardConfig.rows;

    this.rowsObservable.pipe(
      mergeMap(i => range(0, this._gameboardConfig.cols).pipe(
        map(j => [i, j])
      )),
      tap(pair => {
        const i = pair[0];
        const j = pair[1];
        currentColumn = (j === 0) ? startingColumn / 2 : currentColumn;
        rowWidth = (i > 0 && j === 0) ? rowWidth + incrementRowWith : rowWidth;
      })
    ).subscribe(pair => {
      const i = pair[0];
      const j = pair[1];
      const index = i + ',' + j;
      this._drawsService.fillText(this._gameboardConfig.letters[i][j], currentColumn, rowWidth);
      this.addLetterByPosition(index, currentColumn, rowWidth);
      console.log(index, this._gameboardConfig.letters[i][j], currentColumn, rowWidth)
      currentColumn += startingColumn;
    });
  }

  translate(nextWord: string[]): string {
    const word: string = nextWord.reduce((accumulator, currentValue) => {
      const separatorPos = currentValue.indexOf(',');
      if (separatorPos !== -1) {
        const firstIndex = +currentValue.substr(0, separatorPos);
        const secondIndex = +currentValue.substr(separatorPos + 1);
        accumulator += this._gameboardConfig.letters[firstIndex][secondIndex];
      }
      return accumulator;
    }, '');

    return word;
  }

  arrayDifference(arr1, arr2) {
    let arr = [];
    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) {
        arr.push(arr1[i]);
      }
    }

    return arr;
  }

}
