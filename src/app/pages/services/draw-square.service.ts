import { Injectable } from '@angular/core';
import { DrawService } from './draw.service';

@Injectable({
  providedIn: 'root'
})
export class DrawSquareService extends DrawService {
  
  constructor(context) {
    super(context);
  }

  drawSquare(x: number, y: number, moveToY: number, lineToY: number): void {
    this.context.beginPath();
    this.context.moveTo(x, moveToY);
    this.context.lineTo(y, lineToY);
    this.context.stroke();
    this.context.closePath();
  }

  highlightSquare(font: string, style: string, text: string, 
    x: number, y: number): void {
    this.setFontStyle(font);
    this.setFillStyle(style);
    this.fillText(text, x, y);
  }

}
