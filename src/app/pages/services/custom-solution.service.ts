import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomSolutionService {

  constructor() {

  }

  getVariables(variablesStr: string) {
    const variablesStrAsArray = variablesStr.split(',');
    const variables: string[][] = [];
    const buffer = [];
    const separator = '=';
    for (const variableStr of variablesStrAsArray) {
      let pos = variableStr.indexOf(separator);
      let variable = variableStr.substr(0, pos).trim();
      if (!variable) continue;
      let value = variableStr.substr(pos + 1).trim();
      let regExp = Object.keys(buffer).length ? new RegExp('[^a-zA-Z]+(' + Object.keys(buffer).join('|') + ')') : '';
      let inBufferVals = regExp ? value.match(regExp) : null;
      if (inBufferVals !== null) {
        value = value.replace(inBufferVals[1], buffer[inBufferVals[1]]);
      }
      buffer[variable] = eval(value);
      variables.push([variable, eval(value)]);
    }

    return variables;
  }

  getFnc(fnc: string, variables: string[]): object {
    let generatorFnc = '';
    let fncSearch = 'function';
    let funcStart = fnc.indexOf(fncSearch);
    if (funcStart === -1) {
      throw new Error('adas');
    }
    if (fnc[funcStart + fncSearch.length] !== '*') {
      fnc = fnc.replace(/function/, fncSearch + '*');
    }
    let funcTitleMatchs = fnc.match(/function\* ([a-zA-Z]+)?/);
    if (funcTitleMatchs === null) {

    }
    let funcTitle = funcTitleMatchs[1];
    const indexStartParentheses = fnc.indexOf('(');
    const indexEndParentheses = fnc.indexOf(')');
    let param = fnc.substring(indexStartParentheses, indexEndParentheses + 1);
    fnc = fnc.replace(/function(\*)* .+{/, '');
    let indexOfFunctionCall = fnc.lastIndexOf(funcTitle);
    let fncPartFirst = fnc.substr(0, indexOfFunctionCall);
    fncPartFirst += 'yield *' + 'this.act';
    fncPartFirst += fnc.substr(indexOfFunctionCall + funcTitle.length);
    generatorFnc = fncPartFirst;

    /*const paramsArray = params.split(',');
    const functionCall = [];
    for (let param of paramsArray) {
      const arr = param.split(' = ')
      functionCall.push(arr[0] + ' = ' + arr[1]);
    }*/
    for (let key in variables) {
      console.log(key)
    //  generatorFnc = generatorFnc.replace(new RegExp('(?<=[^a-zA-Z])(' + key + ')', 'g'), "this.watches['" + key + "']");
    }

    return {
      'fnc': generatorFnc,
      'params': param
    };
  }
}
