export interface GameboardConfig {
    letters: string[][];
    lettersByPosition: string[];
    rows: number;
    cols: number;
    size: number;
    width: number;
    x: number;
    y: number;
    rectangleWidth: number;
}