import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { from, of } from 'rxjs';
import { delay, concatMap, startWith, pairwise, switchMap } from 'rxjs/operators';
import { GameboardComponent } from '../../gameboard/gameboard.component';
import { DebuggerData } from '../models/debugger-data';
import { AnimationVelocityComponent } from '../../animation-velocity/animation-velocity.component';
import { DebuggerComponent } from '../../debugger/debugger.component';

@Component({
  selector: 'ngx-groupings',
  templateUrl: './groupings.component.html',
  styleUrls: ['./groupings.component.scss']
})
export class GroupingsComponent extends GameboardComponent implements OnInit, AfterViewInit {
  @ViewChild(DebuggerComponent) private dbcComponent: DebuggerComponent;
  @ViewChild(AnimationVelocityComponent) private avComponent: AnimationVelocityComponent;

  n = 8;
  half = this.n / 2;
  k = this.half;
  men: number[] = Array.from(new Array(this.half), (val, index) => (index + 1));
  women: number[] = Array.from(new Array(this.half), (val, index) => (this.half + index + 1));
  relations: Map<number, number>;
  connectedCouples: { [type: string]: Set<number> };

  ngOnInit() {
    const letters = [
      [...this.men], 
      [...this.women]
    ];
    this.setLettersData(letters);
    this.setUsedArray();
    this.width = 200;
    this.draw('230px', '411px');

    this.connectedCouples = {};
    this.connectedCouples["men"] = new Set<number>();
    this.connectedCouples["women"] = new Set<number>();

    this.relations = new Map<number, number>();
    for (let i = 0; i < this.men.length; i++) {
      this.relations[this.men[i]] = this.women[i];
    }
  }

  ngAfterViewInit(): void {
    this.groupCouples();
  }

  private groupCouples() {
    console.log(this)
    const generatorGroupsObservable = from(this.group(0, 0));
    generatorGroupsObservable
      .pipe(
        startWith([]),
        pairwise(),
        switchMap(([previous, curr]) => {
          const currPreviousDiff = this.drawServiceMediator.arrayDifference(curr, previous);
          const current = this.colour(currPreviousDiff, 'lightskyblue');
          const previosCurrDiff = this.drawServiceMediator.arrayDifference(previous, curr).reverse();
          const prev = this.colour(previosCurrDiff, 'black');
          const concat = prev.concat(current);
          return from(concat);
        }),
        concatMap((groupedCouples) => of(groupedCouples).pipe(delay(+this.avComponent.speed * 1000)))
      ).subscribe((square) => {
        console.log(square);
        let separatorPos = square.position.indexOf(',');
        const row = +square.position.substr(0, separatorPos);
        const col = +square.position.substr(++separatorPos);
        const color = (square.colour === 'lightskyblue') ? 'blue' : square.colour;
        const curr = new DebuggerData(row, col, color);
        this.dbcComponent.pushToDebugData(curr);
        this.drawServiceMediator.highlightWords(square.position, square.colour);
      })
  }

  *group(manIndex: number, womanIndex: number) {
    if (this.connectedCouples["men"].size < this.k / 2) {
      for (let i = manIndex; i < this.men.length; i++) {
        if (!this.connectedCouples["men"].has(this.men[i])) {
          this.connectedCouples["men"].add(this.men[i]);
          yield* this.group(i + 1, womanIndex);
          this.connectedCouples["men"].delete(this.men[i]);
        }
      }
    } else if (this.connectedCouples["women"].size < this.k / 2) {
      for (let i = womanIndex; i < this.women.length; i++) {
        const isWomanTaken: boolean = Array.from(this.connectedCouples["men"])
          .some(man => this.relations[man] == this.women[i]);
          if (!this.connectedCouples["women"].has(this.women[i]) && !isWomanTaken) {
            this.connectedCouples["women"].add(this.women[i]);
            yield* this.group(manIndex, i + 1);
            this.connectedCouples["women"].delete(this.women[i]);
          }
      }
    } else {
      const grouped = new Array<string>();
      for (const key in this.connectedCouples) {
        for (const people of this.connectedCouples[key]) {
          const isWoman = people > this.half;
          const row = (isWoman) ? 1 : 0;
          const col = (isWoman) ? people - this.half : people;
          grouped.push([row, col - 1].join()); 
        }
      }

      yield grouped;
    }
  }

}
