export class Square {
    position: string;
    colour: string;

    constructor(position: string, colour: string) {
        this.position = position;
        this.colour = colour;
    }
}