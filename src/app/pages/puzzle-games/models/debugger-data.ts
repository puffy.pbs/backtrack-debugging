export class DebuggerData {
    row: number;
    col: number;
    value: any;

    constructor(row: number, col: number, value: any) {
        this.row = row;
        this.col = col;
        this.value = value;
    }
}