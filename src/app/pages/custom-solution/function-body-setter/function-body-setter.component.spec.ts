import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionBodySetterComponent } from './function-body-setter.component';

describe('FunctionBodySetterComponent', () => {
  let component: FunctionBodySetterComponent;
  let fixture: ComponentFixture<FunctionBodySetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionBodySetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionBodySetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
