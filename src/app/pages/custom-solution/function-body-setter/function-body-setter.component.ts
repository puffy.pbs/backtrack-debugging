import { Component, OnInit, ViewChild } from '@angular/core';

import './ckeditor.loader';
import 'ckeditor';

@Component({
  selector: 'ngx-function-body-setter',
  templateUrl: './function-body-setter.component.html',
  styleUrls: ['./function-body-setter.component.scss']
})
export class FunctionBodySetterComponent implements OnInit {
  @ViewChild('#cke_editor1') editor;

  constructor() { }

  ngOnInit() {
    console.log(this.editor)
  }

}
