import { Component, ViewChild } from '@angular/core';

import { CustomSolutionService } from '../services/custom-solution.service';
import { from, of } from 'rxjs';
import { GameboardComponent } from '../gameboard/gameboard.component';
import { startWith, pairwise, switchMap, concatMap, delay } from 'rxjs/operators';
import { DebuggerData } from '../puzzle-games/models/debugger-data';
import { AnimationVelocityComponent } from '../animation-velocity/animation-velocity.component';
import { DebuggerComponent } from '../debugger/debugger.component';

@Component({
  selector: 'ngx-custom-solution',
  templateUrl: './custom-solution.component.html',
  styleUrls: ['./custom-solution.component.scss']
})
export class CustomSolutionComponent extends GameboardComponent /*implements OnInit, AfterViewInit*/ {
  @ViewChild(DebuggerComponent) private dbcComponent: DebuggerComponent;
  @ViewChild(AnimationVelocityComponent) private avComponent: AnimationVelocityComponent;

  private variables: any[] = [];
  private customSolutionService: CustomSolutionService = new CustomSolutionService();

  setFunctionInit(functionString: string) {
    const delimiter = '---';
    const delimiterPos = functionString.indexOf(delimiter);
    if (delimiterPos !== -1) {
      const functionInit = functionString.substr(0, delimiterPos).trim();
      const generator = functionString.substr(delimiterPos + delimiter.length).trim();
      const variables = this.customSolutionService.getVariables(functionInit);
      this.setVariables(variables);
      const letters = Array.from(new Array(this.variables['n']), (val, index) => index + 1);
      this.setLettersData([
        [...letters]
      ]);
      const functionVals = this.customSolutionService.getFnc(generator, this.variables);
      eval('this.act = function* ' + functionVals['params'] + ' { ' + functionVals['fnc']);
      this.width = 100;
      this.draw(this.width * 2 + 'px', (this.width * this.variables['n'] + 12) + 'px');
      this.runGenerator();
    }
  }

  setVariables(variables: string[][]) {
    for (const [key, value] of variables) {
      this.variables[key] = value;
    }
  }

  *act () {

  }

  runGenerator() {
    const generatorCustomObservable = from(this.act());

    generatorCustomObservable
      .pipe(
        startWith([]),
        pairwise(),
        switchMap(([previous, curr]) => {
          const currPreviousDiff = this.drawServiceMediator.arrayDifference(curr, previous);
          const current = this.colour(currPreviousDiff, 'lightskyblue');
          const previosCurrDiff = this.drawServiceMediator.arrayDifference(previous, curr).reverse();
          const prev = this.colour(previosCurrDiff, 'black');
          const concat = prev.concat(current);
          return from(concat);
        }),
        concatMap((groupedCouples) => of(groupedCouples).pipe(delay(+this.avComponent.speed * 1000)))
      ).subscribe((square) => {
        const row = 0;
        const col = +square.position;
        const color = (square.colour === 'lightskyblue') ? 'blue' : square.colour;
        const curr = new DebuggerData(row, col, color);
        this.dbcComponent.pushToDebugData(curr);
        this.drawServiceMediator.highlightWords(row + ',' + col, square.colour);
      })
  }
  
}
