import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { NbListModule, NbDialogModule } from '@nebular/theme';
import { CKEditorModule } from 'ng2-ckeditor';

import { PuzzleGamesModule } from '../puzzle-games/puzzle-games.module';

//components
import { CustomSolutionService } from '../services/custom-solution.service';
import { CustomSolutionComponent } from './custom-solution.component';
import { FunctionBodySetterComponent } from './function-body-setter/function-body-setter.component';

const COMPONENTS = [
    CustomSolutionComponent,
    FunctionBodySetterComponent
];

const SERVICES = [
    CustomSolutionService
];

const MODULES = [
  ThemeModule,
  NbListModule,
  CKEditorModule,
  PuzzleGamesModule,
  NbDialogModule.forChild()
];

@NgModule({
  imports: MODULES,
  declarations: COMPONENTS,
  providers: SERVICES,
})
export class CustomSolutionModule { }
