import { TestBed } from '@angular/core/testing';

import { LexicaService } from './lexica.service';

describe('LexicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LexicaService = TestBed.get(LexicaService);
    expect(service).toBeTruthy();
  });
});
